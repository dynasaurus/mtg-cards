import sys, json, datetime, time

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from dateutil.parser import parse
from collections import OrderedDict
from cards.models import CardSet, Booster, BoosterSlot, Card, PrintedCard, Supertypes, Types, Subtypes, CardNames, ForeignNames, Variations, Legalities, Rulings, Printings

class Command(BaseCommand):
    help = 'Loads initial data set into the database'

    def add_arguments(self, parser):
        parser.add_argument('file_name', type=str)

    def handle(self, *args, **options):
        with open(options['file_name']) as data_file:
            data = json.load(data_file, object_pairs_hook=OrderedDict)

        counts = populate_db(data)

        self.stdout.write('\nWrote %s cards'   % counts["cards"])
        self.stdout.write('\t%s unique cards'  % counts["printed_cards"])
        self.stdout.write('\t%s sets'          % counts["sets"])
        self.stdout.write('\t%s booster slots' % counts["booster_slots"])

def date_validate(date_text):
    MIN_YEAR = datetime.datetime(datetime.MINYEAR, 1, 1)
    date = parse(date_text, default=MIN_YEAR).date()
    if date != MIN_YEAR.date():
        return date
    else:
        return None

def populate_db(obj):
    
    count              = 0
    unique_count       = 0
    total_cards        = 0
    total_sets         = len(obj.keys())
    set_count          = 0
    booster_slot_count = 0

    for key in obj.keys():
        with transaction.atomic():
            try:
                card_set = obj[key]

                card_set_obj, set_was_created = CardSet.objects.get_or_create(
                    name                  = card_set["name"],
                    code                  = card_set["code"], 
                    gatherer_code         = card_set.get("gathererCode", ""), 
                    old_code              = card_set.get("oldCode"           , ""),
                    magic_cards_info_code = card_set.get("magicCardsInfoCode", ""),
                    release_date          = card_set["releaseDate"],
                    border                = card_set["border"],
                    set_type              = card_set["type"],
                    block                 = card_set.get("block"     , ""),
                    online_only           = card_set.get("onlineOnly", ""),
                )

                set_count = set_count + 1
                total_cards = total_cards + len(card_set["cards"])

                if set_was_created:
                    for booster in card_set.get("booster", ""):
                        booster_slot_obj = Booster.objects.create(
                            card_set = card_set_obj
                        )
                        if not isinstance(booster, str):
                            for booster_slot in booster:
                                BoosterSlot.objects.create(
                                    booster      = booster_slot_obj, 
                                    booster_slot = booster_slot)
                                booster_slot_count = booster_slot_count + 1
                        else:
                            BoosterSlot.objects.create(
                                booster      = booster_slot_obj, 
                                booster_slot = booster)
                            booster_slot_count = booster_slot_count + 1
                    
                sys.stdout.write('\r')
                sys.stdout.write("%s set of %s [%s]      " % (set_count, total_sets, card_set["code"]))
                sys.stdout.flush()

            except Exception:
                sys.stdout.write('\nFailed on %s [%s]'           % (card_set["name"], card_set["code"]))
                sys.stdout.write('\n\tgatherer_code: %s'         % card_set.get("gathererCode",       ""))
                sys.stdout.write('\n\told_code: %s'              % card_set.get("oldCode",            ""))
                sys.stdout.write('\n\tmagic_cards_info_code: %s' % card_set.get("magicCardsInfoCode", ""))
                sys.stdout.write('\n\trelease_date: %s'          % card_set["releaseDate"])
                sys.stdout.write('\n\tborder: %s'                % card_set["border"])
                sys.stdout.write('\n\tset_type: %s'              % card_set["type"])
                sys.stdout.write('\n\tblock: %s'                 % card_set.get("block",      ""))
                sys.stdout.write('\n\tonline_only: %s'           % card_set.get("onlineOnly", ""))
                sys.stdout.write('\n')
                raise

    for key in obj.keys():
        card_set = obj[key]

        card_set_obj = CardSet.objects.get(code = card_set["code"])

        cards = card_set["cards"] 

        with transaction.atomic():
            for card in cards:
                try:
                    card_name = card["name"]
                    mana_cost = card.get("manaCost", "")
                    card_obj, card_was_created = Card.objects.get_or_create(
                        #           card_id, layout, name, 
                        #           mana_cost, cmc, colors, type_text, text, power, 
                        #           toughness, loyalty, hand, life, reserved)
                        layout         = card["layout"],
                        name           = card["name"],
                        mana_cost      = card.get("manaCost", 0),
                        cmc            = card.get("cmc"     , 0),
                        colors         = card.get("colors"  , ['Colorless']),
                        color_identity = card.get("colorIdentity", ['Colorless']),
                        type_text      = card["type"],
                        text           = card.get("text"    , ""),

                        power     = card.get("power"    , ""),
                        toughness = card.get("toughness", ""),
                        loyalty   = card.get("loyalty"  , 0),

                        hand      = card.get("hand"    , 0),
                        life      = card.get("life"    , 0),
                        reserved  = card.get("reserved", False),
                    )

                    for name in card.get("names", ""):
                        CardNames.objects.get_or_create(
                            card = card_obj, 
                            name = name)

                    for supertype_name in card.get("supertypes", ""):
                        Supertypes.objects.get_or_create(
                            card           = card_obj, 
                            supertype_name = supertype_name)

                    for type_name in card.get("types", ""):
                        Types.objects.get_or_create(
                            card      = card_obj, 
                            type_name = type_name)

                    for subtype_name in card.get("subtypes", ""):
                        Subtypes.objects.get_or_create(
                            card         = card_obj, 
                            subtype_name = subtype_name)

                    printed_card_obj, printed_card_was_created = PrintedCard.objects.get_or_create(
                        card              = card_obj, 
                        card_set          = card_set_obj,
                        card_id_hash      = card["id"],
                        original_text     = card.get("originalText", ""), 
                        original_type     = card.get("originalType", ""), 
                        flavor            = card.get("flavor"      , ""),
                        rarity            = card["rarity"], 
                        artist            = card["artist"],
                        number            = card.get("number"      , ""),
                        multiverse_id     = card.get("multiverseid", 0 ), 
                        image_name        = card["imageName"],
                        watermark         = card.get("watermark"   , ""),
                        border            = card.get("border"      , card_set["border"]),
                        timeshifted       = card.get("timeshifted" , ""),
                        release_date      = date_validate(card.get("releaseDate", card_set["releaseDate"])),
                        release_date_text = card.get("releaseDate" , ""),
                        starter           = card.get("starter"     , ""),
                        source            = card.get("source"      , ""),
                    )

                    for foreign_name in card.get("foreignNames", ""):
                        ForeignNames.objects.get_or_create(
                            card          = printed_card_obj,
                            language      = foreign_name["language"],
                            name          = foreign_name["name"],
                            multiverse_id = foreign_name.get("multiverseid", 0),
                        )
                    
                    for variation in card.get("variations", ""):
                        Variations.objects.get_or_create(
                            card          = printed_card_obj,
                            multiverse_id = variation
                        )

                    for get_legality in card.get("legalities", ""):
                        Legalities.objects.get_or_create(
                            card        = card_obj, 
                            game_format = get_legality["format"],
                            legality    = get_legality["legality"],
                        )

                    for ruling in card.get("rulings", ""):
                        Rulings.objects.get_or_create(
                            card        = card_obj,
                            date = ruling["date"],
                            text = ruling["text"],
                        )

                    for printing in card.get("printings", ""):
                        Printings.objects.get_or_create(
                            card     = card_obj,
                            set_code = CardSet.objects.get(code=printing)
                        )

                    count = count + 1
                    if card_was_created:
                        unique_count = unique_count + 1

                    sys.stdout.write('\r')
                    sys.stdout.write("%s cards of %s (%s unique) [%s]      " % (count, total_cards, unique_count, card_set["code"]))
                    sys.stdout.flush()
                except Exception:
                    sys.stdout.write('\nFailed on %s [%s]' % (card["name"], card_set["code"]))
                    sys.stdout.write('\n')
                    raise
    return {
        "cards"        : unique_count,
        "printed_cards": count,
        "sets"         : set_count,
        "booster_slots": booster_slot_count,
    }
