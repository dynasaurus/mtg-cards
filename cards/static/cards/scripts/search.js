var app = angular.module('cards', ['ngResource', 'ngMaterial']);

app.controller('search', function($scope, $resource, $q, $timeout, $mdSidenav) {

    $scope.cardName = "";
    $scope.setName = "";
    $scope.typeName = "";

    $scope.colors = function() {
        var color = 0;
        if($scope.whiteMana) {
            color += 1;
        }
        if($scope.blueMana) {
            color += 2;
        }
        if($scope.blackMana) {
            color += 4;
        }
        if($scope.redMana) {
            color += 8;
        }
        if($scope.greenMana) {
            color += 16;
        }
        if(color == 0 && !$scope.colorlessMana) {
            color = "";
        }
        return color;
    };

    $scope.search = function() {
        var setCode = "";

        if($scope.selectedSet) {
            setCode = $scope.selectedSet.code;
        }

        var resource = $resource('/cards/api/cards/?format=json&ordering=-id'
            + '&name_auto=' + $scope.cardName
            + '&sets__contains=' + setCode
            + '&colors=' + $scope.colors()
            + '&types__contains='+ ($scope.typeName || ''));

        $scope.cardList = resource.get();
    };

    $scope.getSets = function() {
        var resource = $resource('/cards/api/sets/?format=json');
        $scope.sets = resource.get();
    };

    $scope.getTypes = function() {
        var resource = $resource('/cards/api/types/?format=json');
        $scope.types = resource.query();
    };

    $scope.getSets();
    $scope.getTypes();

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(set) {
        return (angular.lowercase(set.name).indexOf(lowercaseQuery) === 0);
      };
    }

    $scope.querySearch = function(query, items) {
        var results = query ? items.filter( createFilterFor(query) ) : items,
          deferred;
        $scope.search();
        return results;
    };

   function createFilterForArray(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(a) {
        return (angular.lowercase(a).indexOf(lowercaseQuery) === 0);
      };
    }

    $scope.querySearchArray = function(query, items) {
        var results = query ? items.filter( createFilterForArray(query) ) : items,
          deferred;
        $scope.search();
        return results;
    }

    $scope.selectedItemChange = function(item) {
        $scope.search();
    };
    $scope.searchTextChange = function(setName) {
        $scope.search();
    }; 

    $scope.add = function() {
        $scope.cardName = tabComplete();
        $mdSidenav('left').toggle();
    };

    $scope.toggleLeft = function() {
        $mdSidenav('left').toggle();
    };

    // Tab completion for card names when adding to list
    // This is not implemented yet
    function tabComplete() {
        names = [];
        for(var i = 0; i < $scope.cardList.results.length; i++) {
            names.push($scope.cardList.results[i].name);
        }

        completedText = sharedStart(names);

        if($scope.cardName.length > completedText.length) {
            return $scope.cardName;
        }
        else {
            return completedText;
        }
    };

    function sharedStart(array){
        var A = array.concat().sort(),
        a1 = A[0], a2 = A[A.length-1], L = a1.length, i = 0;
        while(i < L && a1.charAt(i) === a2.charAt(i)) i++;
        return a1.substring(0, i);
    };
});
