from drf_haystack.serializers import HaystackSerializer, HaystackSerializerMixin
from rest_framework import serializers

from .search_indexes import CardIndex
from .models import Card, Color, CardSet, Types

import json

class ColorsField(serializers.Field):
    def to_representation(self, value):
        return Color.from_db_value(Color, value, None, None, None)

    def to_internal_value(self, data):
        return Color.get_prep_value(Color, value=data)

class PrintedCardField(serializers.Field):
    def to_representation(self, obj):
        return json.loads(obj)

    def to_internal_value(self, data):
        return json.dumps(data)

class CardSearchSerializer(HaystackSerializer):

    colors = ColorsField()
    color_identity = ColorsField()

    supertypes = serializers.JSONField()
    types = serializers.JSONField()
    subtypes = serializers.JSONField()

    printed_cards = PrintedCardField()

    sets = serializers.JSONField()
    rarity = serializers.JSONField()

    class Meta:
        index_classes = [CardIndex]
        fields = [
            "id", "name", "sort_name", "name_auto", "colors", "color_identity", "cmc", "layout", "supertypes", "types", "subtypes", "printed_cards", "sets", "rarity", "get_absolute_url"
        ]
        #ignore_fields = ["name_auto"]

class CardSetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CardSet
        fields = ('code', 'name', 'block', 'release_date')
