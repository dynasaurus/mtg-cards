from haystack import indexes
from backends.elasticsearch import CharField
from cards.models import Card, Color

from more_itertools import unique_everseen

import json, collections

class CardIndex(indexes.SearchIndex, indexes.Indexable):
    id = indexes.IntegerField(model_attr='pk')
    name = indexes.CharField(document=True, model_attr='name')
    sort_name = indexes.CharField(model_attr='name')
    name_auto = indexes.EdgeNgramField(model_attr='name')
    cmc = indexes.IntegerField(model_attr='cmc')

    colors = indexes.IntegerField()
    def prepare_colors(self, obj):
        return Color.get_prep_value(Color, value=obj.colors)

    color_identity = indexes.IntegerField()
    def prepare_color_identity(self, obj):
        return Color.get_prep_value(Color, value=obj.color_identity)

    supertypes = indexes.MultiValueField()
    def prepare_supertypes(self, obj):
        return list(obj.supertypes_set.values_list('supertype_name', flat=True))

    types = indexes.MultiValueField()
    def prepare_types(self, obj):
        return list(obj.types_set.values_list('type_name', flat=True))

    subtypes = indexes.MultiValueField()
    def prepare_subtypes(self, obj):
        return list(obj.subtypes_set.values_list('subtype_name', flat=True))

    sets = indexes.MultiValueField()
    def prepare_sets(self, obj):
        return list(unique_everseen(obj.printedcard_set.values_list('card_set_id', flat=True)))

    layout = indexes.CharField(model_attr='layout')

    rarity = indexes.MultiValueField()
    def prepare_rarity(self, obj):
        return sorted(set(unique_everseen(obj.printedcard_set.values_list('rarity', flat=True))))

    get_absolute_url = indexes.CharField(model_attr='get_absolute_url', indexed=False)

    # printed_cards = CharField(analyzer="snowball")
    printed_cards = indexes.CharField()
    def prepare_printed_cards(self, obj):
        card_list = []
        for card in obj.printedcard_set.all():
            card_dict = {}

            try:
                card_dict = next(l for l in card_list if l['set'] == card.card_set.pk)
            except StopIteration:
                card_dict = {}

            card_dict['set'] = card.card_set.pk
            card_dict['number'] = card.number
            card_dict['release_date'] = card.release_date.strftime('%Y-%m-%d')
            card_dict['border'] = card.border

            try:
                card_dict['img_url'].append(card.img_url())
            except KeyError:
                card_dict['img_url'] = [card.img_url()]

            card_list.append(card_dict)
        return json.dumps(card_list)

    def get_model(self):
        return Card

