from django import template
from django.utils.safestring import mark_safe
from django.template.defaultfilters import stringfilter
from django.templatetags.static import static

register = template.Library()

size="16"

def mana_url_str(url_name, alt_text_name):
    url_str = static('images/symbols/%s.png' % url_name)
    return '<img src="%s" alt="%s" height="%spx" />' % (url_str, alt_text_name, size)

str_replacements = (
    ('{1/2}', mana_url_str("h" , "Half"      )),
    ('{hw}' , mana_url_str("hw", "Half-White")),
    ('{hr}' , mana_url_str("hw", "Half-Red"  )),

    ('{W/U}', mana_url_str("wu", "White or Blue" )),
    ('{W/B}', mana_url_str("wb", "White or Black")),
    ('{U/B}', mana_url_str("ub", "Blue or Black" )),
    ('{U/R}', mana_url_str("ur", "Blue or Red"   )),
    ('{B/R}', mana_url_str("br", "Black or Red"  )),
    ('{B/G}', mana_url_str("bg", "Black or Green")),
    ('{R/W}', mana_url_str("rw", "Red or White"  )),
    ('{R/G}', mana_url_str("rg", "Red or Green"  )),
    ('{G/W}', mana_url_str("gw", "Green or White")),
    ('{G/U}', mana_url_str("gu", "Green or Blue" )),

    ('{2/W}', mana_url_str("2w", "2 or White")),
    ('{2/U}', mana_url_str("2u", "2 or Blue" )),
    ('{2/B}', mana_url_str("2b", "2 or Black")),
    ('{2/R}', mana_url_str("2r", "2 or Red"  )),
    ('{2/G}', mana_url_str("2g", "2 or Green")),

    ('{W/P}', mana_url_str("wp", "Phyrexian White")),
    ('{U/P}', mana_url_str("up", "Phyrexian Blue" )),
    ('{B/P}', mana_url_str("bp", "Phyrexian Black")),
    ('{R/P}', mana_url_str("rp", "Phyrexian Red"  )),
    ('{G/P}', mana_url_str("gp", "Phyrexian Green")),

    ('{W}', mana_url_str("w", "White")),
    ('{U}', mana_url_str("u", "Blue" )),
    ('{B}', mana_url_str("b", "Black")),
    ('{R}', mana_url_str("r", "Red"  )),
    ('{G}', mana_url_str("g", "Green")),

    ('{∞}', mana_url_str("infinity", "Infinity")),
    ('{1000000}', mana_url_str("1000000", "1000000")),
    ('{100}', mana_url_str("100", "100")),

    ('{20}', mana_url_str("20", "20" )),
    ('{19}', mana_url_str("19", "19" )),
    ('{18}', mana_url_str("18", "18" )),
    ('{17}', mana_url_str("17", "17" )),
    ('{16}', mana_url_str("16", "16" )),
    ('{15}', mana_url_str("15", "15" )),
    ('{14}', mana_url_str("14", "14" )),
    ('{13}', mana_url_str("13", "13" )),
    ('{12}', mana_url_str("12", "12" )),
    ('{11}', mana_url_str("11", "11" )),
    ('{10}', mana_url_str("10", "10" )),
    ('{9}', mana_url_str("9", "9"    )),
    ('{8}', mana_url_str("8", "8"    )),
    ('{7}', mana_url_str("7", "7"    )),
    ('{6}', mana_url_str("6", "6"    )),
    ('{5}', mana_url_str("5", "5"    )),
    ('{4}', mana_url_str("4", "4"    )),
    ('{3}', mana_url_str("3", "3"    )),
    ('{2}', mana_url_str("2", "2"    )),
    ('{1}', mana_url_str("1", "1"    )),
    ('{0}', mana_url_str("0", "0"    )),

    ('{X}', mana_url_str("x", "X"        )),
    ('{Y}', mana_url_str("y", "Y"        )),
    ('{Z}', mana_url_str("z", "Z"        )),
    ('{T}', mana_url_str("t", "Tap"      )),
    ('{Q}', mana_url_str("q", "Untap"    )),
    ('{P}', mana_url_str("p", "Phyrexian")),
    ('{S}', mana_url_str("s", "Snow"     )),
    ('CHAOS', mana_url_str("c", "Chaos"    )),
)

color_replacements = (
    ('White', mana_url_str("w", "White")),
    ('Blue' , mana_url_str("u", "Blue" )),
    ('Black', mana_url_str("b", "Black")),
    ('Red'  , mana_url_str("r", "Red"  )),
    ('Green', mana_url_str("g", "Green")),
)

@register.filter(is_safe=True)
@stringfilter
def mana_cost_to_img(value):
    for mana_symbol, mana_url in str_replacements:
        value = value.replace(mana_symbol, mana_url)
    return mark_safe(value)

@register.filter(is_safe=True)
@stringfilter
def rules_text_to_html(value):
    for mana_symbol, mana_url in str_replacements:
        value = value.replace(mana_symbol, mana_url)
    value = value.replace("\n", "<br />")
    return mark_safe(value)

@register.filter(is_safe=True)
@stringfilter
def color_to_img(value):
    for color_name, color_url in color_replacements:
        value = value.replace(color_name, color_url)
    return mark_safe(value)
