from django.db import models
from .color import Color

class Card(models.Model):
    LAYOUT_CHOICES = (
        ('normal'      , 'Normal'),
        ('split'       , 'Split'),
        ('flip'        , 'Flip'),
        ('double-faced', 'Double-faced'),
        ('token'       , 'Token'),
        ('plane'       , 'Plane'),
        ('scheme'      , 'Scheme'),
        ('phenomenon'  , 'Phenomenon'),
        ('leveler'     , 'Leveler'),
        ('vanguard'    , 'Vanguard'),
    )
    layout = models.CharField(max_length=40,
                              choices=LAYOUT_CHOICES, 
                              default='normal')

    name = models.CharField(max_length=150, default="")

    mana_cost = models.CharField(max_length=64, default=0)
    cmc = models.PositiveIntegerField(verbose_name="CMC", default=0)
    colors = Color()
    color_identity = Color()
   
    type_text = models.CharField(max_length=255, default="")

    # Current longest oracle text is 544 characters
    text = models.CharField(max_length=1024, default="", blank=True)

    # Sometimes power and toughness can be *
    power = models.CharField(max_length=8, default="", blank=True)
    toughness = models.CharField(max_length=8, default="", blank=True)
    
    # Only set for Planeswalkers
    loyalty = models.PositiveSmallIntegerField(default=0, blank=True)

    # Only set for Vanguard cards
    hand = models.SmallIntegerField(default=0, blank=True)
    life = models.SmallIntegerField(default=0, blank=True)

    # True if on the reserved list (cannot be reprinted)
    reserved = models.BooleanField(default=False)

    class Meta:
        ordering = ['-pk']

    def get_absolute_url(self):
        return "/cards/%i" % self.id

    def __str__(self):
        return self.name
