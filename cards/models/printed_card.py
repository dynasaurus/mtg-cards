from django.db import models
from .card import Card
from .card_set import CardSet
from . import const

class PrintedCard(models.Model):
    card = models.ForeignKey(Card)
    card_set = models.ForeignKey(CardSet)
    card_id_hash  = models.CharField(max_length=40  , default="", unique=True)
    original_text = models.CharField(max_length=1024, default="", blank=True)
    original_type = models.CharField(max_length=255 , default="", blank=True)
    flavor        = models.CharField(max_length=1024, default="", blank=True)
    RARITY_CHOICES = (
        ('M'  , 'Mythic Rare'),
        ('R'  , 'Rare'       ),
        ('U'  , 'Uncommon'   ),
        ('C'  , 'Common'     ),
        ('L'  , 'Land'       ),
        ('S'  , 'Special'    ),
        ('P'  , 'Promo'      ),
        ('B'  , 'Bonus'      ),
    )
    rarity = models.CharField(max_length=12, choices=RARITY_CHOICES, default="")
    artist = models.CharField(max_length=64, default="")
    number = models.CharField(max_length=8 , default="", blank=True)
    multiverse_id = models.PositiveIntegerField(default=0)
    image_name    = models.CharField(max_length=255, default="")
    watermark     = models.CharField(max_length=64, default="")
    border = models.CharField(max_length=6, 
                              choices=const.BORDER_CHOICES, 
                              default='black')
    # Set to true if a timeshifted card (only for the Time Spiral set)
    timeshifted  = models.BooleanField(default=False)
    # Release date cannot be a DateTime field
    # because sometimes only the month is given
    release_date      = models.DateField(blank=True, null=True)
    release_date_text = models.CharField(max_length=10, default="", blank=True)
    # Set to true if released as part of a core set box (not in boosters)
    starter = models.BooleanField(default=False)
    # Where the card was originally obtained if it was a promo card
    source  = models.CharField(max_length=255, default="", blank=True)

    def img_url(self):
        return "%s/%s.jpg" % (self.card_set.pk, self.image_name)

    class Meta:
        ordering = ['-release_date', 'card_set__code', 'image_name']

    def __str__(self):
        return self.card_set.name
