from django.db import models

class Color(models.fields.PositiveSmallIntegerField):
    description = "The color of a Magic card"

    choices_dict = {
        'Colorless': 0,
        'White'    : 1,
        'Blue'     : 2,
        'Black'    : 4,
        'Red'      : 8,
        'Green'    :16,
    }
    
    choices_id_dict = {
        'Colorless': 0,
        'White'    : 1,
        'W'        : 1,
        'Blue'     : 2,
        'U'        : 2,
        'Black'    : 4,
        'B'        : 4,
        'Red'      : 8,
        'R'        : 8,
        'Green'    :16,
        'G'        :16,
    }

    choices_form = (
        ('Colorless', 'Colorless'),
        ('White'    , 'White'    ),
        ('Blue'     , 'Blue'     ),
        ('Black'    , 'Black'    ),
        ('Red'      , 'Red'      ),
        ('Green'    , 'Green'    ),
    )

    # Take ['White', 'Red'] and convert to 01001 binary (9 in base 10)
    # Also assuming Big Endianess but that shouldn't be super relevant
    def get_prep_value(self, value):
        bitmask = 0
        if value != None:
            for color in value:
               bitmask += self.choices_id_dict[color]
        return bitmask

    # Take 01101 binary and convert to ['Blue', 'Black', 'Green']
    def to_python(self, value):
        return value

    def from_db_value(self, value, expression, connection, context):
        colors = []
        if value == 0: colors = ['Colorless']
        else:
            for choice in self.choices_dict:
                if (self.choices_dict[choice] & value): #Then it is that color
                    colors.append(choice)
        return colors

    def formfield(self, **kwargs):
        from cards.forms import ColorForm
        return ColorForm(
            choices = self.choices_form,
        )
