from django.db import models
from . import const

class CardSet(models.Model):
    name          = models.CharField(max_length=64, default="", unique=True)
    
    code          = models.CharField(max_length=8, primary_key=True)
    gatherer_code = models.CharField(max_length=8, default="")
    old_code      = models.CharField(max_length=8, default="")
    magic_cards_info_code = models.CharField(max_length=8, default="")
    
    release_date = models.DateField()
    border = models.CharField(max_length=6, 
                              choices=const.BORDER_CHOICES,
                              default='black')
    SET_TYPE_CHOICES = (
       ('core'          , 'Core'          ),
       ('expansion'     , 'Expansion'     ),
       ('reprint'       , 'Reprint'       ),
       ('box'           , 'Box'           ),
       ('un'            , 'Un'            ),
       ('from the vault', 'From the Vault'),
       ('premium deck'  , 'Premium Deck'  ),
       ('duel deck'     , 'Duel Deck'     ),
       ('starter'       , 'Starter'       ),
       ('commander'     , 'Commander'     ),
       ('planechase'    , 'Planechase'    ),
       ('archenemy'     , 'Archenemy'     ),
       ('promo'         , 'Promo'         ),
       ('vanguard'      , 'Vanguard'      ),
       ('masters'       , 'Masters'       ),
    )
    set_type = models.CharField(max_length=16,
                                choices   =SET_TYPE_CHOICES,
                                default   ='expansion')
    block       = models.CharField(max_length=32, default="")
    online_only = models.BooleanField(default=False)

    def __str__(self):
         return self.name
