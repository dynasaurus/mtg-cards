from django.db import models
from .card import Card
from .card_set import CardSet
from .printed_card import PrintedCard

class Legalities(models.Model):
    card = models.ForeignKey(Card)
    game_format = models.CharField(max_length=64, default="")
    LEGALITY_CHOICES = (
        ('L', 'Legal'),
        ('R', 'Restricted'),
        ('B', 'Banned'),
    )
    legality = models.CharField(max_length=16, default="")

class Rulings(models.Model): 
    card = models.ForeignKey(Card)
    date = models.DateField()
    text = models.CharField(max_length=2047, default="")

class Printings(models.Model):
    card     = models.ForeignKey(Card)
    set_code = models.ForeignKey(CardSet)

class CardNames(models.Model):
    card = models.ForeignKey(Card)
    name = models.CharField(max_length=255, default="")

class ForeignNames(models.Model):
    card = models.ForeignKey(PrintedCard)
    LANGUAGE_CHOICES = (
        ('Standard', (
            ('en'     , 'English'),
            ('zh-HANT', 'Chinese Traditional'),
            ('de'     , 'German'),
            ('fr'     , 'French'),
            ('it'     , 'Italian'),
            ('ja'     , 'Japanese'),
            ('ko'     , 'Korean'),
            ('pt-BR'  , 'Portuguese (Brazil)'),
            ('ru'     , 'Russian'),
            ('zh-HANS', 'Chinese Simplified'),
            ('es'     , 'Spanish'),
            )
        ),
        ('Promo', (
            ('he'      , 'Hebrew'),
            ('cyrillic', 'Cyrillic'),
            ('grc'     , 'Classic Greek'),
            )
        )
    )
    language      = models.CharField(max_length=32,
                                     choices=LANGUAGE_CHOICES,
                                     default='en')
    name          = models.CharField(max_length=255, default="")
    multiverse_id = models.PositiveIntegerField(default=0)

class Variations(models.Model):
    card = models.ForeignKey(PrintedCard)
    multiverse_id = models.PositiveIntegerField(default=0)

