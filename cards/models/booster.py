from django.db import models
from .card_set import CardSet

class Booster(models.Model):
    card_set = models.ForeignKey(CardSet)

class BoosterSlot(models.Model):
    booster = models.ForeignKey(Booster)
    # Common types:
    #    common
    #    uncommon
    #    rare
    #    mythic rare
    #    land
    #    marketing
    #    checklist
    #    double faced
    # Time spiral:
    #    timeshifted common
    #    timeshifted uncommon
    #    timeshifted rare
    #    timeshifted purple
    # Conspiracy:
    #    draft-matters
    # Vintage Masters:
    #    power nine
    #    foil
    # Modern Masters:
    #    foil common
    #    foil uncommon
    #    foil rare
    #    foil mythic rare
    # Cards marked as "starter: true" should be exempted from booster generation
    booster_slot = models.CharField(max_length=32, default="")
