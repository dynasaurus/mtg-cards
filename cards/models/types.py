from django.db import models
from .card import Card

class Supertypes(models.Model):
    card = models.ForeignKey(Card)
    SUPERTYPE_CHOICES = ( 
        ('Basic'    , 'Basic'    ),
        ('Legendary', 'Legendary'),
        ('Ongoing'  , 'Ongoing'  ),
        ('Snow'     , 'Snow'     ),
        ('World'    , 'World'    ),
    )
    supertype_name = models.CharField(max_length=12,
                                      choices=SUPERTYPE_CHOICES,
                                      default="")
    def __str__(self):
        return self.supertype_name

class Types(models.Model):
    card = models.ForeignKey(Card)
    TYPE_CHOICES = (
        ('Base', (
            ('Artifact'    , 'Artifact'    ),
            ('Creature'    , 'Creature'    ),
            ('Emblem'      , 'Emblem'      ),
            ('Enchantment' , 'Enchantment' ),
            ('Instant'     , 'Instant'     ),
            ('Land'        , 'Land'        ),
            ('Planeswalker', 'Planeswalker'),
            ('Tribal'      , 'Tribal'      ),
            ('Sorcery'     , 'Sorcery'     ),
            )
        ),
        ('Other', (
            ('Conspiracy', 'Conspiracy'),
            ('Phenomenon', 'Phenomenon'),
            ('Plane'     , 'Plane'     ),
            ('Scheme'    , 'Scheme'    ),
            ('Vanguard'  , 'Vanguard'  ),
            )
        )
    )
    type_name = models.CharField(max_length=12,
                                 choices=TYPE_CHOICES,
                                 default="")
    def __str__(self):
        return self.type_name

class Subtypes(models.Model):
    card = models.ForeignKey(Card)
    subtype_name = models.CharField(max_length=31)
    def __str__(self):
        return self.subtype_name
