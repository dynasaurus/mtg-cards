from django import forms
from django.utils.safestring import mark_safe

class ColorForm(forms.MultipleChoiceField):
    widget = forms.CheckboxSelectMultiple
