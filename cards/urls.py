from django.conf.urls import patterns, url, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'cards', views.CardRESTView, base_name='cards-api')
router.register(r'sets', views.CardSetRESTView, base_name='sets-api')
#router.register(r'types', views.TypeRESTView, base_name='types-api')

urlpatterns = [
    # ex: /cards/search/
    url(r'^search/$', views.SearchView.as_view(), name='search'),
    # ex: /cards/id/{id}/
    url(r'^(?P<pk>[0-9]+)$', views.CardView.as_view(), name='card'),
    # Django Rest Framework URLs
    url(r'^api/', include(router.urls)),
    url(r'^api/types', views.TypeRESTView.as_view(), name='types')
]

