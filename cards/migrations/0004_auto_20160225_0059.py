# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 00:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0003_auto_20160225_0057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cardset',
            name='code',
            field=models.CharField(max_length=8, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='cardset',
            name='gatherer_code',
            field=models.CharField(default='', max_length=8),
        ),
        migrations.AlterField(
            model_name='cardset',
            name='magic_cards_info_code',
            field=models.CharField(default='', max_length=8),
        ),
        migrations.AlterField(
            model_name='cardset',
            name='old_code',
            field=models.CharField(default='', max_length=8),
        ),
    ]
