# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 01:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0006_auto_20160225_0101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='foreignnames',
            name='language',
            field=models.CharField(choices=[('Standard', (('en', 'English'), ('zh-HANT', 'Chinese Traditional'), ('de', 'German'), ('fr', 'French'), ('it', 'Italian'), ('ja', 'Japanese'), ('ko', 'Korean'), ('pt-BR', 'Portuguese (Brazil)'), ('ru', 'Russian'), ('zh-HANS', 'Chinese Simplified'), ('es', 'Spanish'))), ('Promo', (('he', 'Hebrew'), ('cyrillic', 'Cyrillic'), ('grc', 'Classic Greek')))], default='en', max_length=32),
        ),
    ]
