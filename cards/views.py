from django.shortcuts import render
from django.views import generic

from haystack.generic_views import SearchView
from rest_framework import generics, views, viewsets, pagination
from rest_framework import response, status, serializers
from rest_framework import authentication, permissions, filters
from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackAutocompleteFilter

from .models import Card, CardSet, Types
from .serializers import CardSearchSerializer, CardSetSerializer

class SearchView(SearchView):
    template_name = 'search/search.html'

class CardView(generic.DetailView):
    model = Card
    template_name = 'cards/card.html'

class CardRESTView(HaystackViewSet):
    index_models = [Card]
    serializer_class = CardSearchSerializer
    filter_backends = [HaystackAutocompleteFilter, filters.OrderingFilter]
    load_all = True

class AllResultsSetPagination(pagination.PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 0

class CardSetRESTView(viewsets.ModelViewSet):
    queryset = CardSet.objects.all().order_by('-release_date')
    serializer_class = CardSetSerializer
    pagination_class = AllResultsSetPagination

class TypeRESTView(views.APIView):
    def get(self, request, format=None):
        types = [value[0] for value in sum([category[1] for category in Types.TYPE_CHOICES], ())]
        return response.Response(types)
