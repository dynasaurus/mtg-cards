from django.conf import settings
from haystack.backends.elasticsearch_backend import ElasticsearchSearchBackend, ElasticsearchSearchEngine, \
    FIELD_MAPPINGS ,  DEFAULT_FIELD_MAPPING
from haystack.constants  import  DJANGO_CT, DJANGO_ID 
from haystack.fields import CharField as BaseCharField

FIELD_MAPPINGS [ 'edge_ngram' ]  =  {
    'type'            :  'string' ,
    'index_analyzer'  :  'edgengram_analyzer' ,
    'search_analyzer' :  'standard'
} 


class ConfigurableFieldMixin(object):

    def __init__(self, **kwargs):
        self.analyzer = kwargs.pop('analyzer', None)
        super(ConfigurableFieldMixin, self).__init__(**kwargs)

class CharField(ConfigurableFieldMixin, BaseCharField):
    pass

class ConfigurableElasticBackend(ElasticsearchSearchBackend):

    DEFAULT_ANALYZER = "snowball"

    def __init__(self, connection_alias, **connection_options):
        super(ConfigurableElasticBackend, self).__init__(
                                connection_alias, **connection_options)
        user_settings = getattr(settings, 'ELASTICSEARCH_INDEX_SETTINGS')
        if user_settings:
            setattr(self, 'DEFAULT_SETTINGS', user_settings)

    def build_schema(self, fields):
        content_field_name, mapping = super(ConfigurableElasticBackend,
                                              self).build_schema(fields)
        mapping.update({ 
            DJANGO_CT :  {
                'type' :  'string' ,
                'index' :  'not_analyzed' ,
                'include_in_all' :  False
                }, 
            DJANGO_ID :  {
                'type' :  'string' , 
                'index' :  'not_analyzed' , 
                'include_in_all' :  False
                }, 
        })

        for field_name, field_class in fields.items():
            field_mapping = mapping[field_class.index_fieldname]

            if field_mapping['type'] == 'string' and field_class.indexed:
                if not hasattr(field_class, 'facet_for') and not \
                                  field_class.field_type in('ngram', 'edge_ngram'):
                    field_mapping['analyzer'] = getattr(field_class, 'analyzer',
                                                            self.DEFAULT_ANALYZER)

            # Do this burden to override `text` fields.
            field_mapping  =  FIELD_MAPPINGS.get(field_class.field_type , DEFAULT_FIELD_MAPPING).copy()

            if field_class.document is True: 
                content_field_name  =  field_class.index_fieldname
                
            if field_mapping['type'] == 'string': 
                if field_class.indexed  is False or hasattr(field_class, 'facet_for'): 
                    field_mapping['index'] = 'not_analyzed'
                    del field_mapping['analyzer'] 

            mapping.update({field_class.index_fieldname: field_mapping})
        return (content_field_name, mapping)

class ConfigurableElasticSearchEngine(ElasticsearchSearchEngine):
    backend = ConfigurableElasticBackend