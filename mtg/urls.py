"""mtg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^cards/', include('cards.urls' , namespace='cards')),
    #url(r'^'      , include(urls)),
    #url(r'^'      , include('accounts.urls', namespace='accounts')),
    #url(r'^login/', views.login, name="login"),
    #url(r'^login/' , include('login.urls' , namespace="login")),
    #url(r'^logout/', include('logout.urls', namespace="logout")),
    url(r'^admin/' , include(admin.site.urls)),
    # redirect to /cards/search/
    url(r'^', RedirectView.as_view(pattern_name='cards:search'))
    #url(r'^', redirect(reverse_lazy('cards:search')))
]
